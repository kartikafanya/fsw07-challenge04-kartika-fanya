class App {
  constructor() {
    this.typeDriver = document.querySelector("#typeDriver");
    this.date = document.querySelector("#date");
    this.time = document.querySelector("#time");
    this.passenger = document.querySelector("#passenger");
    this.btn = document.querySelector(".submit");
    this.carsContainer = document.querySelector(".insert-card-cars");
    this.filter = document.querySelector("#pencarianmu");
    this.navBtn = document.querySelector(".navbar-toggler");
    this.section = document.querySelector(".section-search");
    this.alert = document.querySelector(".alert-mobil");
  }

  async init() {
    if (this.btn !== null) {
      this.btn.onclick = await this.click;
      this.navBtn.onclick = this.navToggler;
    } else {
      this.navBtn.onclick = this.navToggler;
    }
  }

  async loadFilter(filter) {
    const cars = await Database.loadCarsFilter(filter);
    Car.init(cars);
  }

  click = async () => {
    let type = this.typeDriver.options[this.typeDriver.selectedIndex].value;
    let passenger = this.passenger.value;
    let date = this.date.value;
    let time = this.time.value;
    if (type.length !== 0 || date.length !== 0 || time.length !== 0) {
      if (passenger.length === 0) {
        passenger = 0;
      }
      date = new Date(this.date.value);
      await this.loadFilter({ type, passenger, date, time });
    } 
    this.cardRender();
  };

  cardRender() {
    let card = "";
    if (Car.list.length !== 0) {
      Car.list.forEach((car) => {
        card += car.render();
      });
      this.carsContainer.innerHTML = card;
    } else {
      card = `<div class="alert alert-warning alert-dismissible fade show alert-mobil mt-5" role="alert">
                <strong>Mobil tidak tersedia!</strong> silahkan lakukan pencarian lain.
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>`;
      this.alert.innerHTML = card;
    }
  }
}
